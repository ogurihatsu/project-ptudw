<?php

namespace App;

class DangKy
{
	public $mon_hoc = null;
	public $hocphi = 0;

	public function __construct($DaDangKy){
		if($DaDangKy){
			$this->mon_hoc = $DaDangKy->mon_hoc;
			$this->tong_hoc_phi = $DaDangKy->tong_hoc_phi;
		}
	}

	public function add($monhoc, $id_course){
		$DS_DK = ['hoc_phi' => $monhoc->hocphi, 'mon_hoc' => $monhoc];
		if($this->mon_hoc){
			if(array_key_exists($id_course, $this->mon_hoc)){
				$DS_DK = $this->mon_hoc[$id_course];
			}
		}
		$DS_DK['hoc_phi'] = $monhoc->hoc_phi;
		$this->mon_hoc[$id_course] = $DS_DK;
		$this->tong_hoc_phi += $monhoc->hocphi;
	}
}

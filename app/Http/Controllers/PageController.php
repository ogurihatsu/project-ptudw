<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\User;
use App\Courses;
use App\Staff;
use App\Registered_Courses;
use App\DangKy;
use Session;
use Auth;
use Hash;


use Illuminate\Http\Request;

class PageController extends Controller
{
	public function getIndex(){
			return view('master');
	}
    public function getSignup(){
            return view('page.signup');
    }
    public function getUP(){
            return view('page.userpanel');
    }
    public function getAP(){
            return view('page.adminpanel');
    }
    public function postSignup(Request $req){
            $this->validate($req,[
                'name'=>'required',
                'tuoi'=>'required',
                'ngaysinh'=>'required',
                'sdt'=>'required',
                'diachi'=>'required',
                'email'=>'required',
                'password'=>'required',
                'password-confirmation'=>'required|same:password'
            ],
            [
                'name.required'=>'Vui lòng nhập họ tên',
                'tuoi.required'=>'Vui lòng nhập tuổi',
                'ngaysinh.required'=>'Vui lòng nhập ngày sinh',
                'sdt.required'=>'Vui lòng nhập số điện thoại',
                'diachi.required'=>'Vui lòng nhập địa chỉ',
                'email.required'=>'Vui lòng nhập email',
                'password.required'=>'Vui lòng nhập password',
                'password-confirmation.required'=>'Vui lòng nhập lại password',
                'password-confirmation.same'=>'Mật khẩu không giống'
            ]
        );
            $users = new User();
            $users->full_name = $req->name;
            $users->tuoi = $req->tuoi;
            $users->ngay_sinh = $req->ngaysinh;
            $users->sdt = $req->sdt;
            $users->dia_chi = $req->diachi;
            $users->email = $req->email;
            $users->password = Hash::make($req->password);
            $users->save();
            return redirect()->back()->with(['flag'=>'success','message'=>'Đăng ký thành công']);
    }
    public function getLogin(){
    		return view('page.login');
    }
    public function postLogin(Request $req){
    		$this->validate($req,[
    			'email'=>'required',
    			'password'=>'required'
    		],
    		[
    			'email.required'=>'Vui lòng nhập email',
    			'password.required'=>'Vui lòng nhập password'
    		]
        );

            if(Auth::attempt(['email'=>$req->email,'password'=>$req->password,'is_admin'=>1])){
                    return redirect('admin-panel');}
            if(Auth::attempt(['email'=>$req->email,'password'=>$req->password,'is_admin'=>0])){
                    return redirect('user-panel');}    
    		
            else{
                return redirect()->back()->with(['flag'=>'failure','message'=>'Đăng nhập thất bại']);   
            }
    }
    public function getDK(){
        $mon_hoc = Courses::where('trang_thai',1)->get();
        return view('page.dkhp',compact('mon_hoc'));
    }

   public function postDK(Request $req){
            $this->validate($req,[
                'name'=>'required'
                
            ],
            [
                'name.required'=>'Vui lòng nhập lớp muốn đăng ký',
                
            ]
        );
            if(Auth::check()){
                $ten_user = isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->full_name;
            }

            $collection = DB::table('courses')->pluck('ten_mon_hoc');
            if($collection->contains($req->name)){

            $fee = DB::table('courses')->where('ten_mon_hoc',$req->name)->value('hoc_phi');
            $siso = DB::table('courses')->where('ten_mon_hoc',$req->name)->value('si_so');
            $new_siso = $siso+1;
            DB::table('courses')->where('ten_mon_hoc',$req->name)->update(['si_so'=>$new_siso]);
            $data = new Registered_Courses();
            $data->ten_mon_hoc = $req->name;
            $data->full_name = $ten_user;
            $data->hoc_phi = $fee;
            $data->save();
            return redirect()->back()->with(['ketqua'=>'success','message'=>'Đăng ký thành công']);
        }
        else
        {
            return redirect()->back()->with(['ketqua'=>'failure','message'=>'Đăng ký thất bại']);   
        }
    }
    public function getHocphi(){

        $ten_user = isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->full_name;
        $hoc_phi = Registered_Courses::where('full_name',$ten_user)->get();
        $price = DB::table('registered_courses')->where('full_name',$ten_user)->sum('hoc_phi');
        return view('page.hocphi',compact('hoc_phi'),compact('price'));
    }

    public function getThongtin(){
        $ten_mon_hoc = DB::table('courses')->get();
        return view('page.thongtin',compact('ten_mon_hoc'));
    }
    public function getHoadon(){
            return view('page.hoadon');
    }
    public function postHoadon(Request $req){
            $this->validate($req,[
                'name'=>'required'
            ],
            [
                'name.required'=>'Vui lòng nhập tên người dùng'
            ]);
            $collection = DB::table('registered_courses')->pluck('full_name');
            $price = DB::table('registered_courses')->where('full_name',$req->name)->sum('hoc_phi');
            if($collection->contains($req->name)){
                return redirect()->back()->with(['ketqua'=>$req->name,'message'=>$price]);
            }
            else{
                return redirect()->back()->with(['ketqua'=>'failure','message'=>'Tìm thất bại']);
            }
        }
}

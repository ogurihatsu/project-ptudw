-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 04:32 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dkhp`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id_course` int(10) UNSIGNED NOT NULL,
  `ten_mon_hoc` varchar(50) NOT NULL,
  `hoc_phi` int(20) UNSIGNED NOT NULL,
  `si_so` tinyint(5) UNSIGNED NOT NULL DEFAULT '0',
  `trang_thai` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id_course`, `ten_mon_hoc`, `hoc_phi`, `si_so`, `trang_thai`) VALUES
(1, 'Tiếng Anh giao tiếp', 2000000, 0, 1),
(3, 'Tiếng Anh giao tiếp 2', 4000000, 0, 1),
(4, 'Luyện thi TOEIC', 5000000, 0, 1),
(5, 'Luyện thi TOEFL IBT', 10000000, 1, 1),
(6, 'Luyện thi IELTS', 10000000, 2, 1),
(7, 'Luyện thi bằng A', 2000000, 0, 1),
(8, 'Luyện thi bằng B', 3000000, 0, 1),
(9, 'Luyện thi bằng C', 5000000, 0, 1),
(10, 'Luyện thi Cambridge ESOL', 10000000, 0, 1),
(11, 'Cơ bản', 500000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registered_courses`
--

CREATE TABLE `registered_courses` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `ten_mon_hoc` varchar(100) NOT NULL,
  `hoc_phi` int(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registered_courses`
--

INSERT INTO `registered_courses` (`id`, `full_name`, `ten_mon_hoc`, `hoc_phi`, `created_at`, `updated_at`) VALUES
(3, 'Toàn', 'Tiếng Anh giao tiếp', 2000000, '2019-01-16 04:33:09', '2019-01-16 04:33:09'),
(4, 'Toàn', 'Luyện thi TOEFL IBT', 10000000, '2019-01-16 06:06:43', '2019-01-16 06:06:43'),
(5, 'Toàn', 'Luyện thi Cambridge ESOL', 10000000, '2019-01-16 06:53:49', '2019-01-16 06:53:49'),
(6, 'Toàn', 'Luyện thi IELTS', 10000000, '2019-01-16 07:36:44', '2019-01-16 07:36:44'),
(7, 'Nam', 'Luyện thi IELTS', 10000000, '2019-01-16 07:37:57', '2019-01-16 07:37:57'),
(8, 'Toàn', 'Luyện thi TOEFL IBT', 10000000, '2019-01-16 09:25:38', '2019-01-16 09:25:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `tuoi` tinyint(10) UNSIGNED NOT NULL,
  `ngay_sinh` date NOT NULL,
  `sdt` int(10) UNSIGNED NOT NULL,
  `dia_chi` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `remember_token`, `tuoi`, `ngay_sinh`, `sdt`, `dia_chi`, `created_at`, `updated_at`, `is_admin`) VALUES
(3, 'Toàn', 'test1@gmail.com', '$2y$10$xMq/4IX9ZMB7p60qk6/DSuNa952rieVvmrFHfHWVTExatR/XROkyy', NULL, 11, '2019-01-04', 12345678, 'hcm', '2019-01-15 06:42:09', '2019-01-15 06:42:09', 0),
(4, 'Tiến', 'admin@gmail.com', '$2y$10$ofs5Czj375Gh50RLC4Ow1O3h3KfozVO./Ing3UBOT4/lkvI0RpJVu', NULL, 22, '2019-01-03', 123456789, 'hcm', '2019-01-15 07:06:53', '2019-01-15 07:06:53', 1),
(5, 'Nam', 'test2@gmail.com', '$2y$10$znO1ic8xSE.y/GPPn7A/HeJhcxAL2Zp0fVh6bU4c2ol/Cp8JgI4nG', NULL, 20, '1998-10-05', 1234568, 'hn', '2019-01-16 07:17:25', '2019-01-16 07:17:25', 0),
(6, 'Trang', 'test3@gmail.com', '$2y$10$a85j4HkzHIKbXFg3g.4e5uD3F3kF.imS3dmJI8wAmS/IR3PY14kue', NULL, 25, '1994-11-14', 46848676, 'hcm', '2019-01-16 07:20:53', '2019-01-16 07:20:53', 0),
(8, 'tùng', 'test4@gmail.com', '$2y$10$Xozixa6Kay2cddtB0Vtwc.hAkJtNgeOZAXt.cagwJOTc38VBOQhoS', NULL, 26, '1993-04-05', 154630, 'hht', '2019-01-16 07:23:38', '2019-01-16 07:23:38', 0),
(9, 'năm', 'test5@gmail.com', '$2y$10$fmFhORjaWUBuRGA7kHgwVuUaj1mwJoOj1TnoHsbjkXNJu61urAQkm', NULL, 55, '1940-05-14', 4561236, 'cmt8', '2019-01-16 09:26:58', '2019-01-16 09:26:58', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id_course`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered_courses`
--
ALTER TABLE `registered_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id_course` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registered_courses`
--
ALTER TABLE `registered_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

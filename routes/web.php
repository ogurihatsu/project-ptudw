<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('index',[
	'as'=>'index',
	'uses'=>'PageController@getIndex'
]);
Route::get('dang-nhap',[
	'as'=>'login',
	'uses'=>'PageController@getLogin'
]);
Route::post('dang-nhap',[
	'as'=>'login',
	'uses'=>'PageController@postLogin'
]);
Route::get('dang-ky',[
	'as'=>'signup',
	'uses'=>'PageController@getSignup'
]);
Route::post('dang-ky',[
	'as'=>'signup',
	'uses'=>'PageController@postSignup'
]);
Route::get('user-panel',[
	'as'=>'userpanel',
	'uses'=>'PageController@getUP'
]);
Route::get('admin-panel',[
	'as'=>'adminpanel',
	'uses'=>'PageController@getAP'
]);
Route::get('dang-ky-hoc-phan',[
	'as'=>'dkhp',
	'uses'=>'PageController@getDK'
]);
Route::post('dang-ky-hoc-phan',[
	'as'=>'dkhp',
	'uses'=>'PageController@postDK'
]);
Route::get('hoc-phi',[
	'as'=>'hocphi',
	'uses'=>'PageController@getHocphi'
]);
Route::get('thong-tin',[
	'as'=>'thongtin',
	'uses'=>'PageController@getThongtin'
]);
Route::get('hoa-don',[
	'as'=>'hoadon',
	'uses'=>'PageController@getHoadon'
]);
Route::post('hoa-don',[
	'as'=>'hoadon',
	'uses'=>'PageController@postHoadon'
]);
Route::get('thong-tin-hoa-don',[
	'as'=>'HDinfo',
	'uses'=>'PageController@getInfoHD'
]);
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Đăng nhập</title>
    <base href="{{asset('')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

  </head>
<body id="LoginForm">
<div class="container">
<div class="login-form">
<div class="main-div">
    <div class="panel">
   <h2>Đăng nhập</h2>
   <p>Vui lòng nhập email và password</p>
   </div>
    <form action="{{route('login')}}" method="POST">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
        @if(Session::has('flag'))
        <div class="alert alert-{{Session::get('flag')}}">{{Session::get('message')}}</div>
        @endif
        <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Địa chỉ Email" required>

        </div>

        <div class="form-group">

            <input type="password" class="form-control" name="password" placeholder="Password" required>

        </div>
        <button type="submit" class="btn btn-primary">Đăng nhập</button>

    </form>
    </div>
<p class="botto-text">Trung tâm Anh ngữ ILA</p>
</div></div>


</body>
</html>

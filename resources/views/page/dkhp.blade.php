<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Đăng nhập</title>
    <base href="{{asset('')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

</script>

</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar container-fluid">
					<h3 class="mt-3">User Panel</h3>
					<ul class="nav nav-pills flex-column">
						<li class="nav-item">
							<a class="nav-link" href="user-panel">Thông tin cá nhân</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="dang-ky-hoc-phan">Đăng ký học phần</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="hoc-phi">Thông tin học phí</a>
						</li>
					</ul>
				</nav>

				<main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                    <p></p>
					          @if(Auth::check())
                      <li>Welcome <b>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->full_name }}}</b></li>
                    @endif
                    <p>
                        @if(Session::has('ketqua'))
                        <div class="alert alert-{{Session::get('ketqua')}}">{{Session::get('message')}}</div>
                        @endif
                    </p>
					             <section class="row">
                        <p></p>

                        <div class="center container-fluid" style="text-align: left">
						            <h2>Đăng ký học phần</h2></div>
                        <div class="container-fluid">
                        <form class="container-fluid" action="{{route('dkhp')}}" method="POST">
                          <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="col-md-2 field-label-responsive">
                        <label for="name">Môn học đăng ký</label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                                <input type="text" name="name" class="form-control"
                                       placeholder="VD: Tiếng Anh giao tiếp" required>
                            </div>
                        </div>
                    </div>
                            <div class="col-sm-6 field-label-responsive">
                            <input type="submit" name="dangky" value="Đăng ký" id="submit">
                            </div>
                          </form>
                       </div>
                        <div class="table-responsive">
                        <p></p>
                        <table class="table">
                                <tr>
                                  <td></td>
                                  <th scope="row" style="text-align: left" width="20%">Tên môn học</th>
                                  <th scope="row" style="text-align: center">Học phí</th>
                                  <th scope="row" style="text-align: center">Sĩ số</th>
                                  
                                </tr>
                                
                                 @foreach($mon_hoc as $new)
                                <tr>

                                  <form class="container-fluid" action="{{route('dkhp',$new->id_course)}}" method="POST"> 
                                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                  <th scope="row" style="text-align: center">{{$new->id_course}}</th>
                                  <td style="text-align: left" width="15%">{{$new->ten_mon_hoc}}</td>
                                  <td style="text-align: center">{{$new->hoc_phi}}</td>
                                  <td style="text-align: center">{{$new->si_so}}</td>
                                 
                                  </tr>
                                </form>
                                 @endforeach
                                
                          </table>
                          
                        </div>
                    </div>
					</section>
                </p>
				</main>
			</div>
			<hr width="40%">
			<footer class="container-fluid">
					<div class="center" style="text-align: center">
							PHÒNG ĐÀO TẠO ĐẠI HỌC
<br>Phòng A120, Trường Đại học Công nghệ Thông tin.
<br>Khu phố 6, P.Linh Trung, Q.Thủ Đức, TP.Hồ Chí Minh.
<br>Điện thoại: (028) 372 51993, Ext: 113(Hệ từ xa qua mạng), 112(Hệ chính quy).
<br>Email: phongdaotaodh@uit.edu.vn
					</div>
			</footer>
        </div></div>
	</body>
</html>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Đăng nhập</title>
    <base href="{{asset('')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar container-fluid">
					<h3 class="mt-3">User Panel</h3>
					<ul class="nav nav-pills flex-column">
						<li class="nav-item">
							<a class="nav-link" href="user-panel">Thông tin cá nhân</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="dang-ky-hoc-phan">Đăng ký học phần</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="hoc-phi">Thông tin học phí</a>
						</li>
					</ul>
				</nav>

				<main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                    <p></p>
					@if(Auth::check())
                      <li>Welcome <b>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->full_name }}}</b></li>
                    @endif
                    <p></p>
					<section class="row">
                        <p></p> 
                        <div class="center" style="text-align: left">
						<h2>Thông tin cá nhân</h2></div>
                    <div class="table-responsive">
                        <p></p>
                        <table class="table">
                                <tr>
                                  <th scope="row">Họ và tên</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->full_name }}}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Email</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Tuổi</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->tuoi }}}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Ngày sinh</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->ngay_sinh }}}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Số điện thoại</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->sdt }}}</td>
                                </tr>
                                <tr>
                                  <th scope="row">Địa chỉ</th>
                                  <td>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->dia_chi }}}</td>
                                </tr>

                            </table>
                    </div>
					</section>
                </p>
				</main>
			</div>
			<hr width="40%">
			<footer class="container-fluid">
					<div class="center" style="text-align: center">
							PHÒNG ĐÀO TẠO ĐẠI HỌC
<br>Phòng A120, Trường Đại học Công nghệ Thông tin.
<br>Khu phố 6, P.Linh Trung, Q.Thủ Đức, TP.Hồ Chí Minh.
<br>Điện thoại: (028) 372 51993, Ext: 113(Hệ từ xa qua mạng), 112(Hệ chính quy).
<br>Email: phongdaotaodh@uit.edu.vn
					</div>
			</footer>
        </div></div>
	</body>
</html>